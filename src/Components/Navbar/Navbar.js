import {NavLink} from "react-router-dom";
function Navbar(){
    // State
    // Behavior
    // View
    return(
        <nav>
            <NavLink to="/" >Accueil</NavLink>
            <NavLink to="/agencies/bordeaux" >Bordeaux</NavLink>
            <NavLink to="/agencies/grenoble" >Grenoble</NavLink>
            <NavLink to="/agencies/group" >Group</NavLink>
            <NavLink to="/agencies/lyon" >Lyon</NavLink>
            <NavLink to="/agencies/montpellier" >Montpellier</NavLink>
            <NavLink to="/agencies/parisnord" >Paris-Nord</NavLink>
            <NavLink to="/agencies/portland" >Portland</NavLink>
            <NavLink to="/agencies/smp" >Smp</NavLink>
            <NavLink to="/agencies/sogefi" >Sogefi</NavLink>
            <NavLink to="/agencies/stetienne" >St-Etienne</NavLink>
            <NavLink to="/agencies/v2m" >V2m</NavLink>
        </nav>
    );
}

export default Navbar;