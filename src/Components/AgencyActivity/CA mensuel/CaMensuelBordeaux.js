import { useState, useEffect } from "react";

const CaMensuel = (props) => {
  // STATE FACTURES 1 month ago
  const [factureDatabordeaux, setFactureDatabordeaux] = useState([]);
  const [caFactureLastMonthBordeaux, setCaFactureLastMonthBordeaux] = useState(0);
  // STATE AVOIRS 1 month ago
  const [avoirDatabordeaux, setAvoirDatabordeaux] = useState([]);
  const [caAvoirLastMonthBordeaux, setCaAvoirLastMonthBordeaux] = useState(0);
  // CA 1 month ago
  const [caLastMonthBordeaux, setCaLastMonthBordeaux] = useState(0);
 
  // STATE FACTURES 1 month ago last year
  const [factureDataLastYearbordeaux, setFactureDataLastYearbordeaux] = useState([]);
  const [caFactureLastMonthLastYearBordeaux, setCaFactureLastMonthLastYearBordeaux] = useState(0);
  // STATE AVOIRS 1 month ago last year
  const [avoirDataLastYearbordeaux, setAvoirDataLastYearbordeaux] = useState([]);
  const [caAvoirLastMonthLastYearBordeaux, setCaAvoirLastMonthLastYearBordeaux] = useState(0);
  // CA 1 month ago last year
  const [caLastMonthLastYearBordeaux, setCaLastMonthLastYearBordeaux] = useState(0);

  // STATE FACTURES 2 months ago
  const [factureTwoMonthsAgoDatabordeaux, setFactureTwoMonthsAgoDatabordeaux] = useState([]);
  const [caFactureLastTwoMonthsBordeaux, setCaFactureLastTwoMonthsBordeaux] = useState(0);
  // STATE AVOIRS 2 months ago
  const [avoirTwoMonthsAgoDatabordeaux, setAvoirTwoMonthsAgoDatabordeaux] = useState([]);
  const [caAvoirLastTwoMonthsBordeaux, setCaAvoirLastTwoMonthsBordeaux] = useState(0);
  // CA 2 months ago
  const [caLastTwoMonthsBordeaux, setCaLastTwoMonthsBordeaux] = useState(0);
  
  const baseUrl = "http://localhost:3002/api/get";

    // Behavior
    // CA 1 month ago
    useEffect(() => {
        // Factures data
        fetch(`${baseUrl}/bordeaux/facture`) 
        .then(response => response.json())
        .then(data => setFactureDatabordeaux(data));
        
        // Avoirs data
        fetch(`${baseUrl}/bordeaux/avoir`) 
          .then(response => response.json())
          .then(data => setAvoirDatabordeaux(data));
    
        // Total factures
        let ca = 0;
        factureDatabordeaux.forEach((facture) => {
          for (const [key, value] of Object.entries(facture)) {
            if (key === "MTT_TOTAL_HT") {
              ca += value;
            }
          } 
        });
        setCaFactureLastMonthBordeaux(ca);
        
        // Total avoirs
        let caAvoir = 0;
        avoirDatabordeaux.forEach((avoir) => {
          for (const [key, value] of Object.entries(avoir)) {
            if (key === "MTT_TOTAL_HT") {
              caAvoir += value;
            }
          } 
        });
        setCaAvoirLastMonthBordeaux(caAvoir);
        let caLastMonthFacture = caFactureLastMonthBordeaux;
        let caLastMonthAvoir = caAvoirLastMonthBordeaux;
        
        // Total factures - total avoirs
        setCaLastMonthBordeaux(caLastMonthFacture - caLastMonthAvoir);
      }, [factureDatabordeaux, avoirDatabordeaux, caAvoirLastMonthBordeaux, caFactureLastMonthBordeaux]);
    
      // CA 1 month ago and last year
    useEffect(() => {
        // Factures data
        fetch(`${baseUrl}/bordeaux/facture/lastyear`) 
        .then(response => response.json())
        .then(data => setFactureDataLastYearbordeaux(data));
        
        // Avoirs data
        fetch(`${baseUrl}/bordeaux/avoir/lastyear`) 
          .then(response => response.json())
          .then(data => setAvoirDataLastYearbordeaux(data));
    
        // Total factures
        let ca = 0;
        factureDataLastYearbordeaux.forEach((facture) => {
          for (const [key, value] of Object.entries(facture)) {
            if (key === "MTT_TOTAL_HT") {
              ca += value;
            }
          } 
        });
        setCaFactureLastMonthLastYearBordeaux(ca);
        
        // Total avoirs
        let caAvoir = 0;
        avoirDataLastYearbordeaux.forEach((avoir) => {
          for (const [key, value] of Object.entries(avoir)) {
            if (key === "MTT_TOTAL_HT") {
              caAvoir += value;
            }
          } 
        });
        setCaAvoirLastMonthLastYearBordeaux(caAvoir);
        let caLastMonthLastYearFacture = caFactureLastMonthLastYearBordeaux;
        let caLastMonthLastYearAvoir = caAvoirLastMonthLastYearBordeaux;
        
        // Total factures - total avoirs
        setCaLastMonthLastYearBordeaux(caLastMonthLastYearFacture - caLastMonthLastYearAvoir);
      }, [factureDataLastYearbordeaux, avoirDataLastYearbordeaux, caAvoirLastMonthLastYearBordeaux, caFactureLastMonthLastYearBordeaux]);

    // CA 2 months ago
    useEffect(() => {
        // Factures data
        fetch(`${baseUrl}/bordeaux/facture/two-months-ago`) 
        .then(response => response.json())
        .then(data => setFactureTwoMonthsAgoDatabordeaux(data));
        
        // Avoirs data
        fetch(`${baseUrl}/bordeaux/avoir/two-months-ago`) 
          .then(response => response.json())
          .then(data => setAvoirTwoMonthsAgoDatabordeaux(data));
    
        // Total factures
        let ca = 0;
        factureTwoMonthsAgoDatabordeaux.forEach((facture) => {
          for (const [key, value] of Object.entries(facture)) {
            if (key === "MTT_TOTAL_HT") {
              ca += value;
            }
          } 
        });
        setCaFactureLastTwoMonthsBordeaux(ca);
        
        // Total avoirs
        let caAvoir = 0;
        avoirTwoMonthsAgoDatabordeaux.forEach((avoir) => {
          for (const [key, value] of Object.entries(avoir)) {
            if (key === "MTT_TOTAL_HT") {
              caAvoir += value;
            }
          } 
        });
        setCaAvoirLastTwoMonthsBordeaux(caAvoir);
        let caLastTwoMonthsFacture = caFactureLastTwoMonthsBordeaux;
        let caLastTwoMonthsAvoir = caAvoirLastTwoMonthsBordeaux;
        
        // Total factures - total avoirs
        setCaLastTwoMonthsBordeaux(caLastTwoMonthsFacture - caLastTwoMonthsAvoir);
      }, [factureTwoMonthsAgoDatabordeaux, avoirTwoMonthsAgoDatabordeaux, caAvoirLastTwoMonthsBordeaux, caFactureLastTwoMonthsBordeaux]);
  
    function evolution(nb1, nb2){
      let resultat = ((nb1 - nb2) / nb2) * 100;
      resultat = resultat.toFixed(2);
      if (!resultat > 0 ) {
        return`▼ ${resultat}`;
      } 
      return `▲ ${resultat}`;
    }

      // View
    return ( 
        <div  style={{ maxWidth: "30%", textAlign:'center', border: '1px solid', backgroundColor:'lavender', borderRadius:'2%'}} className="container">
            <h3 style={{textAlign:'center'}}>Ca mensuel du mois dernier</h3>
            <p style={{ textAlign:'center', fontWeight: 'bold', fontSize: 24}} className="caMoins1mois">{caLastMonthBordeaux.toFixed(2)} €</p>
            <table style={{ width: "100%"}}>
                <thead>
                  <tr>
                    <th style={{ border: "1px solid"}}>M-1</th>
                    <th style={{ border: "1px solid"}}>N-1</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style={{ width: "50%"}}>Évolution M-1 M-2</td>
                        <td style={{ width: "50%"}}>1 month ago last year</td>
                    </tr>
                    <tr>
                        <td style={{ border: "1px solid", textAlign:'center', fontWeight: 'bold'}}>{evolution(caLastMonthBordeaux, caLastTwoMonthsBordeaux)} %</td>
                        <td style={{ border: "1px solid", textAlign:'center', fontWeight: 'bold'}}>{evolution(caLastMonthBordeaux, caLastMonthLastYearBordeaux)} %</td>
                    </tr>
                </tbody>
            </table>
        </div>
     );
}

export default CaMensuel;