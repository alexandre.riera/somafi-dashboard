import {Outlet} from "react-router-dom";
import Navbar from "../Navbar/Navbar";

function AgencyActivity(){
    // state
    // Behavior
    // View
    return(
        <div>
            <Navbar />
            <Outlet></Outlet>
        </div>
    )
}
export default AgencyActivity;