import { NavLink } from "react-router-dom";

function Agence(props) {
    //State
    const agenceName = props.name;
    const caMoinsUnMois = props.ca;
    const agence_name_color = props.color;
    const urlPassed = props.urlPassed;
    //Behavior
    //style
    const agence_name = {
        textAlign:'center',
        fontWeight:'bold',
        fontSize:34,
        marginTop:10,
        color:agence_name_color,
    }
    //View
    return(
        <div style={agence_card}>
            <NavLink style={{textDecoration: 'none'}} to={urlPassed}>
                <div style={agenceName_container}>
                    <p style={agence_name}><img style={agence_logo} src="./new-bonhomme-somafi.png" alt='logo somafi' />{agenceName}</p>
                </div>
                <div style={ca_container}>
                    <p style={ca}>{caMoinsUnMois} €</p>
                </div>
            </NavLink>
        </div>
    )
}
//Style
const agence_card = {
    height:300,
    width:300,
    border: '1px solid black',
    borderRadius:'15px',
    boxShadow: '4px 4px 6px gray',
    margin:'0 auto',
}
const nav = {
    listStyleType: 'none'
}
const agenceName_container = {
    backgroundColor: 'rgb(0,0,0)',
    height:80,
    width:302,
    paddingTop:1,
    borderTopLeftRadius:'15px',
    borderTopRightRadius:'15px',
    marginTop:-1
}

const agence_logo = {
    width:40,
    marginRight:20,
    marginBottom:-15
}
const ca_container = {
    textAlign: 'center',
    marginTop: 100
}
const ca = {
    textAlign: 'center',
    fontSize:34,
    marginTop:60,
    color:'black',
    listStyleType: 'none'
}
export default Agence;