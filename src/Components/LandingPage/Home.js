import Agence from "./Agence";
import {useState, useEffect} from "react";

function Home(){
    //State
  // STATE FACTURES
  const [factureDatabordeaux, setFactureDatabordeaux] = useState([]);
  const [factureDatagrenoble, setFactureDatagrenoble] = useState([]);
  const [factureDatagroup, setFactureDatagroup] = useState([]);
  const [factureDatalyon, setFactureDatalyon] = useState([]);
  const [factureDatamontpellier, setFactureDatamontpellier] = useState([]);
  const [factureDataparisnord, setFactureDataparisnord] = useState([]);
  const [factureDataportland, setFactureDataportland] = useState([]);
  const [factureDatasmp, setFactureDatasmp] = useState([]);
  const [factureDatasogefi, setFactureDatasogefi] = useState([]);
  const [factureDatastetienne, setFactureDatastetienne] = useState([]);
  const [factureDatav2m, setFactureDatav2m] = useState([]);
  
  const [caFactureLastMonthBordeaux, setCaFactureLastMonthBordeaux] = useState(0);
  const [caFactureLastMonthGrenoble, setCaFactureLastMonthGrenoble] = useState(0);
  const [caFactureLastMonthGroup, setCaFactureLastMonthGroup] = useState(0);
  const [caFactureLastMonthLyon, setCaFactureLastMonthLyon] = useState(0);
  const [caFactureLastMonthMontpellier, setCaFactureLastMonthMontpellier] = useState(0);
  const [caFactureLastMonthParisnord, setCaFactureLastMonthParisnord] = useState(0);
  const [caFactureLastMonthPortland, setCaFactureLastMonthPortland] = useState(0);
  const [caFactureLastMonthSmp, setCaFactureLastMonthSmp] = useState(0);
  const [caFactureLastMonthSogefi, setCaFactureLastMonthSogefi] = useState(0);
  const [caFactureLastMonthStetienne, setCaFactureLastMonthStetienne] = useState(0);
  const [caFactureLastMonthV2m, setCaFactureLastMonthV2m] = useState(0);

  // STATE AVOIRS
  const [avoirDatabordeaux, setAvoirDatabordeaux] = useState([]);
  const [avoirDatagrenoble, setAvoirDatagrenoble] = useState([]);
  const [avoirDatagroup, setAvoirDatagroup] = useState([]);
  const [avoirDatalyon, setAvoirDatalyon] = useState([]);
  const [avoirDatamontpellier, setAvoirDatamontpellier] = useState([]);
  const [avoirDataparisnord, setAvoirDataparisnord] = useState([]);
  const [avoirDataportland, setAvoirDataportland] = useState([]);
  const [avoirDatasmp, setAvoirDatasmp] = useState([]);
  const [avoirDatasogefi, setAvoirDatasogefi] = useState([]);
  const [avoirDatastetienne, setAvoirDatastetienne] = useState([]);
  const [avoirDatav2m, setAvoirDatav2m] = useState([]);
  
  const [caAvoirLastMonthBordeaux, setCaAvoirLastMonthBordeaux] = useState(0);
  const [caAvoirLastMonthGrenoble, setCaAvoirLastMonthGrenoble] = useState(0);
  const [caAvoirLastMonthGroup, setCaAvoirLastMonthGroup] = useState(0);
  const [caAvoirLastMonthLyon, setCaAvoirLastMonthLyon] = useState(0);
  const [caAvoirLastMonthMontpellier, setCaAvoirLastMonthMontpellier] = useState(0);
  const [caAvoirLastMonthParisnord, setCaAvoirLastMonthParisnord] = useState(0);
  const [caAvoirLastMonthPortland, setCaAvoirLastMonthPortland] = useState(0);
  const [caAvoirLastMonthSmp, setCaAvoirLastMonthSmp] = useState(0);
  const [caAvoirLastMonthSogefi, setCaAvoirLastMonthSogefi] = useState(0);
  const [caAvoirLastMonthStetienne, setCaAvoirLastMonthStetienne] = useState(0);
  const [caAvoirLastMonthV2m, setCaAvoirLastMonthV2m] = useState(0);

  // CA LAST MONTH AGENCES
  const [caLastMonthBordeaux, setCaLastMonthBordeaux] = useState(0);
  const [caLastMonthGrenoble, setCaLastMonthGrenoble] = useState(0);
  const [caLastMonthGroup, setCaLastMonthGroup] = useState(0);
  const [caLastMonthLyon, setCaLastMonthLyon] = useState(0);
  const [caLastMonthMontpellier, setCaLastMonthMontpellier] = useState(0);
  const [caLastMonthParisnord, setCaLastMonthParisnord] = useState(0);
  const [caLastMonthPortland, setCaLastMonthPortland] = useState(0);
  const [caLastMonthSmp, setCaLastMonthSmp] = useState(0);
  const [caLastMonthSogefi, setCaLastMonthSogefi] = useState(0);
  const [caLastMonthStetienne, setCaLastMonthStetienne] = useState(0);
  const [caLastMonthV2m, setCaLastMonthV2m] = useState(0);

  const baseUrl = "http://localhost:3002/api/get";

  // ------------------- Behavior
  //--------------------------------------------------- FACTURES ---------------------------------------
  
  // Bordeaux 
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/bordeaux/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatabordeaux(data));
    
    // Avoirs data
    fetch(`${baseUrl}/bordeaux/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatabordeaux(data));

    // Total factures
    let ca = 0;
    factureDatabordeaux.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthBordeaux(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatabordeaux.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthBordeaux(caAvoir);
    let caLastMonthFacture = caFactureLastMonthBordeaux;
    let caLastMonthAvoir = caAvoirLastMonthBordeaux;
    
    // Total factures - total avoirs
    setCaLastMonthBordeaux(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatabordeaux, avoirDatabordeaux, caAvoirLastMonthBordeaux, caFactureLastMonthBordeaux]);
  
  // // GRENOBLE
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/grenoble/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatagrenoble(data));
    
    // Avoirs data
    fetch(`${baseUrl}/grenoble/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatagrenoble(data));

    // Total factures
    let ca = 0;
    factureDatagrenoble.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthGrenoble(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatagrenoble.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthGrenoble(caAvoir);
    let caLastMonthFacture = caFactureLastMonthGrenoble;
    let caLastMonthAvoir = caAvoirLastMonthGrenoble;
    
    // Total factures - total avoirs
    setCaLastMonthGrenoble(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatagrenoble, avoirDatagrenoble, caAvoirLastMonthGrenoble, caFactureLastMonthGrenoble]);
  

  // // GROUP
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/group/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatagroup(data));
    
    // Avoirs data
    fetch(`${baseUrl}/group/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatagroup(data));

    // Total factures
    let ca = 0;
    factureDatagroup.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthGroup(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatagroup.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthGroup(caAvoir);
    let caLastMonthFacture = caFactureLastMonthGroup;
    let caLastMonthAvoir = caAvoirLastMonthGroup;
    
    // Total factures - total avoirs
    setCaLastMonthGroup(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatagroup, avoirDatagroup, caAvoirLastMonthGroup, caFactureLastMonthGroup]);
  

  // // LYON
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/lyon/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatalyon(data));
    
    // Avoirs data
    fetch(`${baseUrl}/lyon/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatalyon(data));

    // Total factures
    let ca = 0;
    factureDatalyon.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthLyon(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatalyon.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthLyon(caAvoir);
    let caLastMonthFacture = caFactureLastMonthLyon;
    let caLastMonthAvoir = caAvoirLastMonthLyon;
    
    // Total factures - total avoirs
    setCaLastMonthLyon(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatalyon, avoirDatalyon, caAvoirLastMonthLyon, caFactureLastMonthLyon]);
  

  // // MONTPELLIER
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/montpellier/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatamontpellier(data));
    
    // Avoirs data
    fetch(`${baseUrl}/montpellier/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatamontpellier(data));

    // Total factures
    let ca = 0;
    factureDatamontpellier.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthMontpellier(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatamontpellier.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthMontpellier(caAvoir);
    let caLastMonthFacture = caFactureLastMonthMontpellier;
    let caLastMonthAvoir = caAvoirLastMonthMontpellier;
    
    // Total factures - total avoirs
    setCaLastMonthMontpellier(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatamontpellier, avoirDatamontpellier, caAvoirLastMonthMontpellier, caFactureLastMonthMontpellier]);
  

  // // PARIS NORD
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/parisnord/facture`) 
    .then(response => response.json())
    .then(data => setFactureDataparisnord(data));
    
    // Avoirs data
    fetch(`${baseUrl}/parisnord/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDataparisnord(data));

    // Total factures
    let ca = 0;
    factureDataparisnord.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthParisnord(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDataparisnord.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthParisnord(caAvoir);
    let caLastMonthFacture = caFactureLastMonthParisnord;
    let caLastMonthAvoir = caAvoirLastMonthParisnord;
    
    // Total factures - total avoirs
    setCaLastMonthParisnord(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDataparisnord, avoirDataparisnord, caAvoirLastMonthParisnord, caFactureLastMonthParisnord]);
  

  // // PORTLAND
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/portland/facture`) 
    .then(response => response.json())
    .then(data => setFactureDataportland(data));
    
    // Avoirs data
    fetch(`${baseUrl}/portland/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDataportland(data));

    // Total factures
    let ca = 0;
    factureDataportland.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthPortland(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDataportland.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthPortland(caAvoir);
    let caLastMonthFacture = caFactureLastMonthPortland;
    let caLastMonthAvoir = caAvoirLastMonthPortland;
    
    // Total factures - total avoirs
    setCaLastMonthPortland(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDataportland, avoirDataportland, caAvoirLastMonthPortland, caFactureLastMonthPortland]);
  

  // // SMP
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/smp/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatasmp(data));
    
    // Avoirs data
    fetch(`${baseUrl}/smp/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatasmp(data));

    // Total factures
    let ca = 0;
    factureDatasmp.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthSmp(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatasmp.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthSmp(caAvoir);
    let caLastMonthFacture = caFactureLastMonthSmp;
    let caLastMonthAvoir = caAvoirLastMonthSmp;
    
    // Total factures - total avoirs
    setCaLastMonthSmp(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatasmp, avoirDatasmp, caAvoirLastMonthSmp, caFactureLastMonthSmp]);
  

  // // SOGEFI
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/sogefi/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatasogefi(data));
    
    // Avoirs data
    fetch(`${baseUrl}/sogefi/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatasogefi(data));

    // Total factures
    let ca = 0;
    factureDatasogefi.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthSogefi(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatasogefi.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthSogefi(caAvoir);
    let caLastMonthFacture = caFactureLastMonthSogefi;
    let caLastMonthAvoir = caAvoirLastMonthSogefi;
    
    // Total factures - total avoirs
    setCaLastMonthSogefi(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatasogefi, avoirDatasogefi, caAvoirLastMonthSogefi, caFactureLastMonthSogefi]);
  

  // // ST ETIENNE
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/stetienne/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatastetienne(data));
    
    // Avoirs data
    fetch(`${baseUrl}/stetienne/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatastetienne(data));

    // Total factures
    let ca = 0;
    factureDatastetienne.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthStetienne(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatastetienne.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthStetienne(caAvoir);
    let caLastMonthFacture = caFactureLastMonthStetienne;
    let caLastMonthAvoir = caAvoirLastMonthStetienne;
    
    // Total factures - total avoirs
    setCaLastMonthStetienne(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatastetienne, avoirDatastetienne, caAvoirLastMonthStetienne, caFactureLastMonthStetienne]);
  
  // // V2M
  useEffect(() => {
    // Factures data
    fetch(`${baseUrl}/v2m/facture`) 
    .then(response => response.json())
    .then(data => setFactureDatav2m(data));
    
    // Avoirs data
    fetch(`${baseUrl}/v2m/avoir`) 
      .then(response => response.json())
      .then(data => setAvoirDatav2m(data));

    // Total factures
    let ca = 0;
    factureDatav2m.forEach((facture) => {
      for (const [key, value] of Object.entries(facture)) {
        if (key === "MTT_TOTAL_HT") {
          ca += value;
        }
      } 
    });
    setCaFactureLastMonthV2m(ca);
    
    // Total avoirs
    let caAvoir = 0;
    avoirDatav2m.forEach((avoir) => {
      for (const [key, value] of Object.entries(avoir)) {
        if (key === "MTT_TOTAL_HT") {
          caAvoir += value;
        }
      } 
    });
    setCaAvoirLastMonthV2m(caAvoir);
    let caLastMonthFacture = caFactureLastMonthV2m;
    let caLastMonthAvoir = caAvoirLastMonthV2m;
    
    // Total factures - total avoirs
    setCaLastMonthV2m(caLastMonthFacture - caLastMonthAvoir);
  }, [factureDatav2m, avoirDatav2m, caAvoirLastMonthV2m, caFactureLastMonthV2m]);
  
  //---------------------------------------------- FIN USEEFFECT FACTURES---------------------------------------------------------
  
  //View
  return (
    <>
      <header>
        <h1 style={titleStyle} className="title">DASHBOARD SOMAFI GROUP FRANCE</h1>
      </header>
      <main> 
        <div style={agence_row}>
          <Agence name='Bordeaux' color='orange' ca={caLastMonthBordeaux.toFixed(2)} urlPassed="/agencies/bordeaux" />
          <Agence name='Grenoble' color='lightblue' ca={caLastMonthGrenoble.toFixed(2)} urlPassed="/agencies/grenoble" />
          <Agence name='Group' color='grey' ca={caLastMonthGroup.toFixed(2)} urlPassed="/agencies/group" />
          <Agence name='Lyon' color='green' ca={caLastMonthLyon.toFixed(2)} urlPassed="/agencies/lyon" />
        </div>
        <div style={agence_row}>
          <Agence name='Montpellier' color='yellow' ca={caLastMonthMontpellier.toFixed(2)} urlPassed="/agencies/montpellier" />
          <Agence name='Paris-Nord' color='violet' ca={caLastMonthParisnord.toFixed(2)} urlPassed="/agencies/parisnord" />
          <Agence name='Portland' color='red' ca={caLastMonthPortland.toFixed(2)} urlPassed="/agencies/portland" />
          <Agence name='SMP' color='gold' ca={caLastMonthSmp.toFixed(2)} urlPassed="/agencies/smp" />
        </div>
        <div style={agence_row}>
          <Agence name='Sogefi' color='red' ca={caLastMonthSogefi.toFixed(2)} urlPassed="/agencies/sogefi" />
          <Agence name='St Etienne' color='orange' ca={caLastMonthStetienne.toFixed(2)} urlPassed="/agencies/stetienne" />
          <Agence name='Toulouse' color='pink' ca={caLastMonthV2m.toFixed(2)} urlPassed="/agencies/v2m" />
        </div>
      </main>
    </>
  );
}

//style
const titleStyle = {
  textAlign:'center',
  marginBottom:30,
}
const agence_row = {
  display: 'flex',
  justifyContent: 'space-around',
  marginTop:30
}
export default Home;