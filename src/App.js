import {Routes, Route} from "react-router-dom";
import AgencyActivity from "./Components/AgencyActivity/AgencyActivity";
import AgenceBordeaux from "./Components/AgencyActivity/Agencies/AgenceBordeaux";
import AgenceGrenoble from "./Components/AgencyActivity/Agencies/AgenceGrenoble";
import AgenceGroup from "./Components/AgencyActivity/Agencies/AgenceGroup";
import AgenceLyon from "./Components/AgencyActivity/Agencies/AgenceLyon";
import AgenceMontpellier from "./Components/AgencyActivity/Agencies/AgenceMontpellier";
import AgenceParisnord from "./Components/AgencyActivity/Agencies/AgenceParisnord";
import AgencePortland from "./Components/AgencyActivity/Agencies/AgencePortland";
import AgenceSmp from "./Components/AgencyActivity/Agencies/AgenceSmp";
import AgenceSogefi from "./Components/AgencyActivity/Agencies/AgenceSogefi";
import AgenceStetienne from "./Components/AgencyActivity/Agencies/AgenceStetienne";
import AgenceV2m from "./Components/AgencyActivity/Agencies/AgenceV2m";
import Home from "./Components/LandingPage/Home";

function App() {

  return(
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/agencies" element={<AgencyActivity />}>
          <Route path="/agencies/bordeaux" element={<AgenceBordeaux />} />
          <Route path="/agencies/grenoble" element={<AgenceGrenoble />} />
          <Route path="/agencies/group" element={<AgenceGroup />} />
          <Route path="/agencies/lyon" element={<AgenceLyon />} />
          <Route path="/agencies/montpellier" element={<AgenceMontpellier />} />
          <Route path="/agencies/parisnord" element={<AgenceParisnord />} />
          <Route path="/agencies/portland" element={<AgencePortland />} />
          <Route path="/agencies/smp" element={<AgenceSmp />} />
          <Route path="/agencies/sogefi" element={<AgenceSogefi />} />
          <Route path="/agencies/stetienne" element={<AgenceStetienne />} />
          <Route path="/agencies/v2m" element={<AgenceV2m />} />
        </Route>
      </Routes>
    </div>
  );
}
export default App;
