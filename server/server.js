const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 3002;
let moment = require('moment');
// All configs DB
const dbBordeaux = require('./config/dbBordeaux');
const dbGrenoble = require('./config/dbGrenoble');
const dbGroup = require('./config/dbGroup');
const dbLyon = require('./config/dbLyon');
const dbMontpellier = require('./config/dbMontpellier');
const dbParisnord = require('./config/dbParisnord');
const dbPortland = require('./config/dbPortland');
const dbSmp = require('./config/dbSmp');
const dbSogefi = require('./config/dbSogefi');
const dbStetienne = require('./config/dbStetienne');
const dbV2m = require('./config/dbToulouse');
const { useEffect } = require('react');

app.use(cors());
app.use(express.json())

// Route to get all documents by name ('facture', 'avoir') between 2 dates with format string 'datemonthday' ex: '20220910'
    // Année en cours | Mois d'avant du 01 au 31

    function caLastMonth(){
        let year = moment().format('YYYY');
        let pastMonth = moment().subtract(1, 'month').format('MM');
        let dateStart = `${year}${pastMonth}01`;
        let dateEnd = `${year}${pastMonth}31`;
        // ------------------------------------------------------ ROUTES FACTURES ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/facture', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/facture', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/facture', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/facture', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/facture', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/facture', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/facture', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/facture', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/facture', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/facture', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/facture', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES FACTURES ----------------------------------------------
        
        // ------------------------------------------------------ ROUTES AVOIRS ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/avoir', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/avoir', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/avoir', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/avoir', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/avoir', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/avoir', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/avoir', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/avoir', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/avoir', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/avoir', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/avoir', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES AVOIRS  ----------------------------------------------
    }
    caLastMonth();

    function ca2MonthsAgo(){
        let year = moment().format('YYYY');
        pastMonth = moment().subtract(2, 'month').format('MM');
        dateStart = `${year}${pastMonth}01`;
        dateEnd = `${year}${pastMonth}31`;
        // ------------------------------------------------------ ROUTES FACTURES ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/facture/two-months-ago', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/facture/two-months-ago', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/facture/two-months-ago', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/facture/two-months-ago', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/facture/two-months-ago', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/facture/two-months-ago', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/facture/two-months-ago', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/facture/two-months-ago', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/facture/two-months-ago', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/facture/two-months-ago', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/facture/two-months-ago', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES FACTURES ----------------------------------------------
        
        // ------------------------------------------------------ ROUTES AVOIRS ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/avoir/two-months-ago', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/avoir/two-months-ago', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/avoir/two-months-ago', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/avoir/two-months-ago', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/avoir/two-months-ago', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/avoir/two-months-ago', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/avoir/two-months-ago', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/avoir/two-months-ago', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/avoir/two-months-ago', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/avoir/two-months-ago', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/avoir/two-months-ago', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES AVOIRS  ----------------------------------------------
    }
    ca2MonthsAgo();

    function caLastMonthLastYear(){
        let year = moment().format('YYYY');
        let lastYear = moment().subtract(1, 'year').format('YYYY');
        let pastMonth = moment().subtract(1, 'month').format('MM');
        let dateStart = `${lastYear}${pastMonth}01`;
        let dateEnd = `${lastYear}${pastMonth}31`;
        // ------------------------------------------------------ ROUTES FACTURES ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/facture/lastyear', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/facture/lastyear', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/facture/lastyear', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/facture/lastyear', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/facture/lastyear', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/facture/lastyear', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/facture/lastyear', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/facture/lastyear', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/facture/lastyear', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/facture/lastyear', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/facture/lastyear', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM FACTURE 
                WHERE FACTURE.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES FACTURES ----------------------------------------------
        
        // ------------------------------------------------------ ROUTES AVOIRS ----------------------------------------------
        // Route GET Bordeaux
        app.get('/api/get/bordeaux/avoir/lastyear', (req,res)=>{
            dbBordeaux.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Grenoble
        app.get('/api/get/grenoble/avoir/lastyear', (req,res)=>{
            dbGrenoble.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Group
        app.get('/api/get/group/avoir/lastyear', (req,res)=>{
            dbGroup.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Lyon
        app.get('/api/get/lyon/avoir/lastyear', (req,res)=>{
            dbLyon.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Montpellier
        app.get('/api/get/montpellier/avoir/lastyear', (req,res)=>{
            dbMontpellier.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Parisnord
        app.get('/api/get/parisnord/avoir/lastyear', (req,res)=>{
            dbParisnord.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Portland
        app.get('/api/get/portland/avoir/lastyear', (req,res)=>{
            dbPortland.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Smp
        app.get('/api/get/smp/avoir/lastyear', (req,res)=>{
            dbSmp.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Sogefi
        app.get('/api/get/sogefi/avoir/lastyear', (req,res)=>{
            dbSogefi.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET Stetienne
        app.get('/api/get/stetienne/avoir/lastyear', (req,res)=>{
            dbStetienne.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        
        // Route GET V2m
        app.get('/api/get/v2m/avoir/lastyear', (req,res)=>{
            dbV2m.query(
                `SELECT MTT_TOTAL_HT, DT_FACTURE 
                FROM AVOIR 
                WHERE AVOIR.DT_FACTURE 
                BETWEEN ${dateStart} 
                AND ${dateEnd}`, (err,result)=>{
                if(err) {
                console.log(err)
                }
                res.send(result)
            });   
        });
        // ------------------------------------------------------ FIN ROUTES AVOIRS  ----------------------------------------------
    }
    caLastMonthLastYear();
app.listen(PORT, ()=>{
    console.log(`Server is running on port ${PORT}`);
})